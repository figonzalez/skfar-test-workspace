import React from "react"
import { useCssHandles } from "vtex.css-handles"
import "./index.css"

const CSS_HANDLES = ["wishlistButtonContainer", "wishlistButtonText"]

const WishlistButton = ({AddListenButton}) => {
    const handles = useCssHandles(CSS_HANDLES)

    const handleClick = () => {
        const wishlistButton = document.querySelector(".vtex-minimumtheme-0-x-wishlistButtonContainer .vtex-button")

        wishlistButton && wishlistButton.click()
    }

    return (
        <div onClick={handleClick} className={`${handles.wishlistButtonContainer}`}>
            <AddListenButton />
            <div className={`${handles.wishlistButtonText}`}>
                Agregar a favoritos
            </div>
        </div>
    )
}

export default WishlistButton